import { Component, OnInit } from '@angular/core';
import {AdminService} from "../../service/Admin.service";
import { Observable } from 'rxjs';

import {ChatServiceService, Message} from "../../service/chat-service.service";
import {scan} from "rxjs/operators";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {

  GetInfo;
  CountUsersVar;
  CountProfVar;
  CountStudentsVar;
  messages: Observable<Message[]>;
  formValue;
  constructor(private AdminService : AdminService, public chat : ChatServiceService) { }

  ngOnInit() {
    this.messages = this.chat.conversation.asObservable()
      .pipe(scan((acc, val) => acc.concat(val) ));
  this.GetUserToken();
  this.GetUserID();
  this.CountUsersFn();
  this.CountProfsFn();
  this.CountStudentFn();
  }

  get isProf() {
    return this.GetUserID().usertype =='Prof';
  }

  sendMessage() {
    if (this.formValue === '') {
      return;
    } else {
      this.chat.converse(this.formValue);
      this.formValue = '';
    }
  }

  get isUser(){
    return this.GetUserID().usertype == 'Student';
  }

  GetUserID(){
    var data = JSON.parse(localStorage.getItem('user'));
    this.GetInfo = data;
    return data;
  }

  GetUserToken(){
    return this.GetInfo = localStorage.getItem('token');
  }

  CountUsersFn(){
    return this.AdminService.GetAllUsers().subscribe(res=>{
      this.CountUsersVar = res;
    })
  }

  CountProfsFn(){
    return this.AdminService.ShowProfList().subscribe(res=>{
      this.CountProfVar = res;
    })
  }

  CountStudentFn(){
    return this.AdminService.ShowStudentsList().subscribe(res=>{
      this.CountStudentsVar = res;
    })
  }
}
