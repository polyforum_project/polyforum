import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfRoutingModule } from './prof-routing.module';
import { ModifierprofComponent } from './modifierprof/modifierprof.component';
import { ConsulterCoursProfComponent } from './consulter-cours-prof/consulter-cours-prof.component';
import { ConsulterPubComponent } from './consulter-pub/consulter-pub.component';
import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";
import {ConsulterPubsComponent} from "../admin/consulter-pubs/consulter-pubs.component";

@NgModule({
  declarations: [  ModifierprofComponent, ConsulterCoursProfComponent, ConsulterPubComponent],
  imports: [
    CommonModule,
    ProfRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers:[ConsulterCoursProfComponent,ModifierprofComponent,ConsulterPubsComponent]
})
export class ProfModule { }
