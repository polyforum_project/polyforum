import { Component, OnInit } from '@angular/core';
import {ProfService} from "../../../service/prof.service";
import Swal from "sweetalert2";
import { AdminService } from 'src/app/service/Admin.service';
import {FormBuilder, Validators, FormsModule, ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'app-consulter-cours-prof',
  templateUrl: './consulter-cours-prof.component.html',
  styleUrls: ['./consulter-cours-prof.component.css']
})
export class ConsulterCoursProfComponent implements OnInit {
  ShowCourseProfVar;
  ShowCourstByIDVar;
  ShowCibleVar;submitted;
  courData;fileToUpload;
  constructor(private ShowCourseProfService :ProfService, private ShowGroupeService: AdminService, private FormBuilder: FormBuilder) { }

  ngOnInit() {
    this.courData = this.FormBuilder.group({
      libelle: ['', Validators.required],
      description: ['', Validators.required],
      group: ['', Validators.required],
    });
    this.ShowCourseForProfFn();
  }
  ShowCourseForProfFn(){
    return this.ShowCourseProfService.ShowCourseListProf().subscribe(res=>{
      this.ShowCourseProfVar = res;
    })
  }
  GetCoursID(id){
    return id;
  }

  uploadCourPub (files : FileList ){
    this.fileToUpload = files.item(0);
    return false ;
  }

  ShowGroupe(){
    return this.ShowGroupeService.ShowGroupsList().subscribe(res=>{
      this.ShowCibleVar = res;
    })
  }

  get f(){
    return this.courData.controls;
  }

  ShowCoursByID(id){
    return this.ShowCourseProfService.ShowCordsCours(this.GetCoursID(id)).subscribe(res=>{
      this.ShowCourstByIDVar = res;
    })
  }

  DeleteCourseFn(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'êtes-vous sûr ?',
      text: "La suppression sera définitive!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Supprimer',
      cancelButtonText: 'Non, Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Supprimé!',
          'Le cour a été supprimé.',
          'success'
        );
        return this.ShowCourseProfService.DeleteCours(id).subscribe(res=>{
          this.ShowCourseForProfFn();
          console.log(res);
        })
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annulé',
          'Suppression est annulée',
          'error'
        )
      }
    })
  }

  OnDataAdd(){
    this.submitted = true;

    if (this.courData.invalid) {
      return;
    }
    this.ShowCourseProfService.AddCourse(this.courData.value, this.fileToUpload).subscribe(res=>{
      console.log("entered1");
      this.submitted=false;
      this.courData.reset();
      this.ShowCourseForProfFn();
      Swal.fire({
        position: 'center',
        type: 'success',
        title: "Publié avec succés",
        showConfirmButton: false,
        timer: 4500
      })
    })
  };

}
