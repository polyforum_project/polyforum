var express= require("express");
var bodyparser=require("body-parser");

var db=require('./Modele/db');
const admin=require('./Router/AdminRouter');
const student=require('./Router/StudentRouter');
const proff=require('./Router/ProfRouter');
const commenttt=require('./Router/CommentsRouter');
const courrr=require('./Router/CoursRouter');
const conta=require('./Router/ContactRouter');
const departmnt=require('./Router/DepartementRouter');
const grp=require('./Router/GroupRouter');
const docmnt=require('./Router/DocRouter');
const doc=require('./Router/DocRouter');
const res=require('./Router/ResultRouter');
const rol=require('./Router/RoleRouter');
const email=require('./Router/EmailRouter');



var app=express();
app.use(bodyparser.urlencoded({extend:false}));
app.use(bodyparser.json());


app.set('secretKey', "Key");
app.use('/admin', admin);
app.use('/student', student);
app.use('/prof',proff);
app.use('/comment',commenttt);
app.use('/cour',courrr);
app.use('/contact',conta);
app.use('/dep',departmnt);
app.use('/group',grp);
app.use('/doc',docmnt);
app.use('/resultt',res);
app.use('/role',rol);
app.use('/email',email);


app.listen(1919,function () {
  console.log("serveur running on 1919 port");
});
